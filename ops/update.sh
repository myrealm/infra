#! /usr/bin/env bash
#set -euo pipefail

DIRS=(
	"toastops"
	"living-systems"
)

for d in ${DIRS[@]};
do
	echo "#################################";
	echo "  ###" $d;
	echo "#################################";
	cd $d
	nix flake update
	nix flake lock
	cd ..
done

cd ..
git add .
git commit -m "update flake"
git push
