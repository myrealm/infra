{ pkgs, networks, machines, ...}:
let
  #n = import ../../network/networks.nix;
  #m = import ../../network/machines.nix;
  n = networks;
  m = machines;
  name = "nanopi";
  priIf = m.${name}.ls.k21;
  priNet = n.ls.k21;

  secIf = m.${name}.ls.nanolan;
  secNet = n.ls.nanolan;

  staticIf = {iFace, network, ... }:
  {
    "${iFace.ifname}" = {
      mtu = 1474;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = { iFace }: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.hostName = name;

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  boot.kernel.sysctl = {
    #"net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
    #"net.ipv6.conf.all.forwarding" = true;
  };

  services.meta-wireguard = {
    enable = true;
    hostName = name;
    instances = [
      {
        site = "ls";
        net = "wg";
      }
    ];
  };

  services.meta-dhcpd = {
    enable = true;
    interfaces = [
      priIf.ifname
      secIf.ifname
    ];
    site = "home";
    #nets = [ "lan" "wifi" "slowfi" "nanolan" ];
    nets = [ "lan" "nanolan" ];
  };

  services.meta-unbound = {
    enable = true;
    interfaces = [
      priIf.ip
      secIf.ip
    ];
    #site = "home";
    #nets = [ "lan" "wifi" "slowfi" "nanolan" ];
    site = "ls";
    nets = [ "k21" "wifi" "slowfi" "nanolan" ];
  };
  #services.unbound.stateDir = "/storage/unbound"; # FIXME not working!!!!

  networking = {
    dhcpcd.enable = false;

    defaultGateway = priNet.gateway;
    #defaultGateway6 = {
    #  address = "2a01:4f8:c2c:3bf1::1";
    #  #metric = 231;
    #  #interface = priIf.ifname;
    #  interface = "wg-ls-wg";
    #};

    nameservers = [ ] ++ priNet.dnsServers;

    interfaces = {
      "wg-ls-wg".mtu = 1390;
      #"${priIf.ifname}" = {
      #  ipv4 = {
      #    addresses = [{
      #      address = priIf.ip;
      #      prefixLength = priNet.length;
      #    }];
      #  };
      #};
      "${priIf.ifname}" = {
        #ipv6 = {
        #  addresses = [{
        #    "address" = "2a01:4f8:c2c:3bf1::3";
        #    "prefixLength" = 64;
        #  }];
        #  #routes = [{
        #  #    address = "fe80::1";
        #  #    prefixLength = 128;
        #  #    options = { scope = "link"; };
        #  #}];
        #};
      };
    }
      // (staticIf { iFace = priIf; network = priNet; }) //
      (staticIf { iFace = secIf; network = secNet; });

    #nat = {
    #  enable = true;
    #  externalInterface = priIf.ifname;
    #  externalIP = priIf.ip;
    #  internalInterfaces = [ secIf.ifname wifiIf.ifname ];
    #  internalIPs = [ secIf.ip wifiIf.ip ];
    #};
  };

  networking.firewall.enable = true;
  networking.firewall = {
    checkReversePath = "strict";
    #logReversePathDrops = true;
    # log gets cluttered with
    # rpfilter drop: IN=wan OUT= MAC=33:33:00:00:00:01:c4:27:95:b1:06:f1:86:dd SRC=fe80:0000:0000:0000:c627:95ff:feb1:06f1 DST=ff02:0000:0000:0000:0000:0000:0000:0001 LEN=160 TC=0 HOPLIMIT=255 FLOWLBL=0 PROTO=ICMPv6 TYPE=134 CODE=0
    # and
    # rpfilter drop: IN=wan OUT= MAC=33:33:00:00:00:01:c4:27:95:b1:06:f1:86:dd SRC=fe80:0000:0000:0000:c627:95ff:feb1:06f1 DST=ff02:0000:0000:0000:0000:0000:0000:0001 LEN=76 TC=0 HOPLIMIT=1 FLOWLBL=0 PROTO=ICMPv6 TYPE=130 CODE=0
    # ... on host nanopi
    # whats going on here?
    logReversePathDrops = false;
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    2049  # NFS
    5432  # phppgadmin
    5555  # ggm-http
    8000  # http-static
    49152 # zpinger
    34057 # zyre testapp
    20048 # zyre testapp
    42639 # zyre testapp
  ];

  networking.firewall.allowedUDPPorts = [
    53    # DNS
    123   # NTP
    111   # SunRPC
    2049  # NFS
    5670  # ZRE-DISC
    43090 # zyre testapp
    59607 # zyre testapp
  ];

  networking.firewall.allowedTCPPortRanges = [
    { from = 6000; to = 6010; } # gilgamesh port range
  ];
}
