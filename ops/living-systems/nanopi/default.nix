{
  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ./myrealm.nix

    ./mosquitto.nix
    #./webhost.nix
    ./sops.nix
    ./gitea.nix
  ];
}
