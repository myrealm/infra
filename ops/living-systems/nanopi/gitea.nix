{ config, pkgs, ... }:
let
  gitport = 3200;
  giteauser = "gitea";
in
{
  networking.firewall.allowedTCPPorts = [ gitport ];

  services.gitea = {
    enable = true;
    user = giteauser;
    appName = "gitea@nanopi";
    stateDir = "/data/gitea";
    #useWizard = true;

    settings = {
      server = {
        DOMAIN = "np.lan";
        HTTP_PORT = gitport;
        HTTP_ADDR = "0.0.0.0";
        ROOT_URL = "http://np.lan:${toString gitport}/";
      };

      mailer = {
        ENABLED = true;
        MAILER_TYPE = "sendmail";
        FROM = "no-reply@living-systems.dev";
        SENDMAIL_PATH = "${pkgs.system-sendmail}/bin/sendmail";
      };

      other = {
        SHOW_FOOTER_VERSION = false;
      };

    };

    database = {
      type = "postgres";
      user = giteauser;
    };
  };
}
