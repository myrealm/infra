{ pkgs, config, ... }:
{
  hardware.firmware = [ pkgs.firmwareLinuxNonfree ];

  # TODO FIXME investigate.. this should be set by the harware module somwhere..
  nixpkgs.system = "aarch64-linux";

  # Select internationalisation properties.
  console.keyMap = "de";
  time.timeZone = "Europe/Amsterdam";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  # mdadm usb stick raid 1
  # split raid1 to rpi and nanopi
  fileSystems = {
    "/storage" = {
      device = "/dev/disk/by-label/storage";
      #device = "/dev/sda1";
      fsType = "ext4";
      options = [ "defaults" "user" "rw" ];
    };
  };
}
