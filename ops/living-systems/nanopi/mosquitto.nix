{
  networking.firewall.allowedTCPPorts = [ 1883 ];

  services.mosquitto = {
    enable = true;
    #checkPasswords = true;
    #allowAnonymous = true;
    listeners = [
      {
        #omitPasswordAuth = true;
        users = {
          "iot" = {
            "password" = "asdf";
            acl = [
              "readwrite iot/#"
              "readwrite hutschi/#"
              "readwrite cmnd/#"
              "readwrite tele/#"
              "readwrite stat/#"
            ];
          };
          "zephyr" = {
            "password" = "zephyr";
            acl = [
              "readwrite zephyr/#"
            ];
          };
        };
      }
    ];
  };
}
