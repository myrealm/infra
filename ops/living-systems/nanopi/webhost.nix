{ config, pkgs, lib, ... }:
let
  storageDir = "/storage/www";
in
{
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  users.users.nginxtest = {
    group = "nginxtest";
    isSystemUser = true;
  };
  users.groups.nginxtest.members = [ "nginxtest" config.services.nginx.user ];

  systemd.tmpfiles.rules = [
    "d ${storageDir}/portal 770 nginxtest nginxtest"
    "d ${storageDir}/phptest 770 nginxtest nginxtest"
    "d ${storageDir}/files 770 nginxtest nginxtest"
  ];

  services.nginx = {
    enable = true;
    virtualHosts."metrics" = {
      listen = [ { addr = "0.0.0.0"; port = 9101; } ];
      locations = {
        "/metrics" = {
          extraConfig = ''
	    stub_status on;
          '';
        };
      };
    };

    virtualHosts."portal" = {
      root = "${storageDir}/portal";
      listen = [ { addr = "0.0.0.0"; port = 80; } ];
    };

    virtualHosts."phptest" = {
      root = "${storageDir}/phptest";
      listen = [ { addr = "0.0.0.0"; port = 9123; } ];
      locations = {
        "/" = {
          extraConfig = ''
            try_files $uri /index.php;
          '';
        };

        "~ \.php$" = {
          extraConfig = ''
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:${config.services.phpfpm.pools.nginxtest.socket};
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
          '';
        };
      };
    };

    virtualHosts."files" = {
      root = "${storageDir}/files";
      listen = [ { addr = "0.0.0.0"; port = 9111; } ];
      locations = {
        "/" = {
          extraConfig = ''
            autoindex on;
          '';
        };
      };
    };
  };

  services.phpfpm.pools.nginxtest = {
    user = "nginxtest";
    group = "nginxtest";
    phpPackage = pkgs.php;
    settings = {
      "listen.owner" = config.services.nginx.user;
      "pm" = "dynamic";
      "pm.max_children" = 16;
      "pm.max_requests" = 100;
      "pm.start_servers" = 2;
      "pm.min_spare_servers" = 2;
      "pm.max_spare_servers" = 4;
      "php_admin_value[error_log]" = "stderr";
      "php_admin_flag[log_errors]" = true;
      "catch_workers_output" = true;
    };
    phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
  };
}
