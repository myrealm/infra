{ config, pkgs, ... }:
let
  pIp = "10.100.0.1";

in
{

  services.prometheus.exporters.postfix = {
    enable = true;
    listenAddress = pIp;
    openFirewall = true;
  };

  # needed:
  # for redis WARNING overcommit_memory is set to 0!
  # Background save may fail under low memory condition.
  boot.kernel.sysctl = {
    "vm.overcommit_memory" = true;
  };


  systemd.tmpfiles.rules = [
    "d /data/mail 770 postfix postfix"
    "d /data/mail/cert 770 postfix postfix"
  ];


  #services.postfix.config."smtpd_tls_security_level" = "encrypt";

  mailserver = {
    enable = true;

    debug = false;

    fqdn = "mail.living-systems.dev";
    domains = [ "living-systems.dev" ];

    # TODO
    openFirewall = true;
    #enableSubmission = true; # default
    enableImap = false;
    enableSubmissionSsl = false;

    # collides with resolved?!
    localDnsResolver = false;

    # A list of all login accounts. To create the password hashes, use
    # nix run nixpkgs.apacheHttpd -c htpasswd -nbB "" "super secret password" | cut -d: -f2
    # ...
    # nix shell nixpkgs#apacheHttpd -c htpasswd -nbB "" "super secret password" | cut -d: -f2 > admin.pw > /root/secrets/ls/mail/admin.pw
    loginAccounts = {
        "admin@living-systems.dev" = {
          name = "living admin";
          #hashedPasswordFile = "/root/secrets/ls/mail/admin.pw";
          hashedPassword = "$2y$05$ivdvw1GZUni6XzB4uFn1IerV9B.eGxTL1g601whQmavaecUhQRW1e";
          aliases = [
            "postmaster@living-systems.dev"
            "abuse@living-systems.dev"
            "support@living-systems.dev"
          ];
        };
        "shop@living-systems.dev" = {
          name = "living support";
          #hashedPasswordFile = "/root/secrets/ls/mail/shop.pw";
          hashedPassword = "$2y$05$7sMCYj8L9rD62QnMmgIuU.Hb3cQrEsRCw452/RiPcTia68p7zto5u";
          #aliases = [
          #];
        };
        "leonard.pollak@living-systems.dev" = {
          name = "leo";
          #hashedPasswordFile = "/root/secrets/ls/mail/leo.pw";
          hashedPassword = "$2y$05$6BrsYiroO3cnl34zwWtcY.nx247Kh.Lgfh5xnOHng7NCQ.uHJLAru";
          aliases = [
            "l@living-systems.dev"
            "lp@living-systems.dev"
            "leo@living-systems.dev"
            "pollak@living-systems.dev"
            "leonardp@living-systems.dev"
          ];
        };
    };

    # Use Let's Encrypt certificates. Note that this needs to set up a stripped
    # down nginx and opens port 80.
    certificateScheme = 1;
    certificateFile = "/data/mail/cert/cert.pem";
    keyFile = "/root/secrets/ls/mail/key.pem";
  };
}
