{ config, pkgs, ... }:
{
  networking.hostName = "proxy";

  # Select internationalisation properties.
  console.keyMap = "de";
  time.timeZone = "Europe/Amsterdam";

  myrealm.profile.enable = true;
  myrealm.profile.hardware = "hetzner";
  myrealm.profile.gui = "headless";
  myrealm.profile.service = "ssh-only";

  myrealm.layer.enable = true;
  myrealm.layer.users = true;
  myrealm.layer.machine = "proxy";
  #myrealm.layer.paths = [
  #  ["ls" "hetzner"]
  #];

  systemd.tmpfiles.rules = [
    "d /acme 770 root root"
    "d /data 770 root root"
  ];

  myrealm.layer.domain.ls = {
    enable = true;
    storage = {
      secret = "/root/secrets";
      www = "/www";
      mut = "/data";
      cert = "/acme";
    };
  };
}
