{ pkgs, networks, machines, ...}:
let
  n = networks;
  m = machines.proxy;

  name = m.name;

  priIf = m.ls.hetzner;
  priNet = n.ls.hetzner;

  staticIf = {iFace, network, ... }:
  {
    "${iFace.ifname}" = {
      #mtu = 1474;
      mtu = 1390;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = { iFace }: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.firewall.enable = true; # !!!

  networking.hostName = name;

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
    "net.ipv6.conf.all.forwarding" = true;
  };

  networking = {
    dhcpcd.enable = false;

    defaultGateway = {
      address = "172.31.1.1";
    };
    defaultGateway6 = {
      address = "fe80::1";
      #metric = 231;
      interface = priIf.ifname;
    };
    #nameservers = [ ] ++ priNet.dnsServers;

    #nat = {
    #  enable = true;
    #  externalInterface = priIf.ifname;
    #  externalIP = priIf.ip;
    #  internalInterfaces = [ secIf.ifname wifiIf.ifname ];
    #  internalIPs = [ secIf.ip wifiIf.ip ];
    #};

    interfaces = {
      "wg-ls-wg" = {
        mtu = 1390;
        #ipv6 = {
        #  addresses = [{
        #    "address" = "2a01:4f8:c2c:3bf1:1::1";
        #    "prefixLength" = 64;
        #  }];
        #  routes = [{
        #      address = "2a01:4f8:c2c:3bf1:1::";
        #      prefixLength = 80;
        #      options = { scope = "link"; };
        #  }];
        #};
      };
      "${priIf.ifname}" = {
        ipv4 = {
          addresses = [{
            "address" = priIf.ip;
            "prefixLength" = 32;
          }];
          routes = [{
              address = "172.31.1.1";
              prefixLength = 32;
              options = { scope = "link"; };
          }];
        };
        #ipv6 = {
        #  addresses = [{
        #    "address" = "2a01:4f8:c2c:3bf1::1";
        #    "prefixLength" = 64;
        #  }];
        #  #routes = [{
        #  #    address = "fe80::1";
        #  #    prefixLength = 128;
        #  #    options = { scope = "link"; };
        #  #}];
        #};
      };
    };
  };

  #networking.firewall.enable = false;
  #networking.firewall.checkReversePath = false;

  services.meta-wg-server = {
    enable = true;
    iface = "enp1s0";
    domain = "ls";
    net = "wg";
    pkiPath = "/root/secrets";
  };
}
