{ config, pkgs, lib, ... }:
{
  environment.systemPackages = with pkgs; [
    wakelan
    iperf2
  ];



  myrealm.profile.enable = true;

  #profile.hardware = "qemu-efi";
  #myrealm.nanopi.enable = true;

  myrealm.profile.gui = "headless";
  myrealm.profile.service = "ssh-only";

  myrealm.layer.enable = true;
  myrealm.layer.users = true;
  myrealm.layer.machine = "rpi";

  #myrealm.layer.paths = [
  #  ["ls" "k21"]
  #];

  # we will need a bind mount for myrealm
  fileSystems."/data" = {
    device = "/storage";
    options = [ "bind" ];
  };

  #systemd.tmpfiles.rules = [
  #  "d /www 770 nginx nginx"
  #  "d /www/acme 770 nginx nginx"
  #];

  myrealm.layer.domain.ls = {
    enable = true;

    globals = {
      build-host.enable = true;
    };

    storage = {
      secret = "/root/secrets";
      www = "/www";
      mut = "/data";
      cert = "/www/acme";
    };
  };
}
