{ config, pkgs, ... }:
{
  imports = [
    ./options.nix
    ./module.nix
  ];
}
