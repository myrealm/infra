{ config, lib, pkgs, ... }:

let
  cfg = config.a2dpsink;
  speaker-agent = pkgs.callPackage ./speaker-agent.nix {};
in {

  # interface
  options.a2dpsink = {
    enable = lib.mkEnableOption "configuration for a2dp sink";
  };

  # module
  config = lib.mkIf cfg.enable {

    systemd.services.a2dpsink = {
      enable = true;
      serviceConfig = {
        ExecStart = "${(pkgs.python3.withPackages (ps: with ps; [ pygobject3 dbus-python ]))}/bin/python3 ${speaker-agent}/speaker-agent.py";
      };
    };

  };

}
