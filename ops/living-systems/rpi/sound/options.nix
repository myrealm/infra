{ pkgs, ... }:
{
  sound.enable = true;

  a2dpsink.enable = true;

  sound.extraConfig = ''
    pcm.!default  {
      type hw card 0
    }
    ctl.!default {
      type hw card 0
    }
  '';

  # Auto Reconnect
  hardware.bluetooth = {
    disabledPlugins = [ "sap" "vcp" "mcp" "bap" ];
    settings = {
      Gerneral = {
        JustWorksRepairing = "always";
        Experimental = "true";
      };
    };
  };


  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    wireplumber.enable = true;
    pulse.enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
  };

}
