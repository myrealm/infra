{ config, lib, pkgs, ... }:
{

  nixpkgs.system = "aarch64-linux";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  #swapDevices = [ { device = "/swapfile"; size=1024; } ];

  fileSystems = {
    "/usbdata" = {
      device = "/dev/disk/by-uuid/3e339201-2dfb-4ed6-bb8e-5f51919690d9";
      fsType = "ext4";
      options = [ "defaults" "nofail" "x-systemd.device-timeout=10"];
    };
    "/var/lib/containers/storage" = {
      device = "/usbdata";
      options = [ "bind" ];
    };
  };
}
