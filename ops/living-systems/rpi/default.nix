{ config, pkgs, ... }:
{
  imports = [
    ./hardware.nix
    ./myrealm.nix
    ./network.nix
    ./packages.nix

    ./sound
  ];
}
