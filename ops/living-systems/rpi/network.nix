{ pkgs, networks, machines, ...}:
let
  n = networks;
  m = machines;

  name = "rpi";

  priIf = m.${name}.ls.k21;
  priNet = n.ls.k21;

  secIf = m.${name}.ls.slowfi;
  secNet = n.ls.slowfi;

  staticIf = {iFace, network, ... }:
  {
    "${iFace.ifname}" = {
      mtu = 1460;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = { iFace }: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.hostName = name;

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
  };

  services.meta-wireguard = {
    enable = true;
    hostName = name;
    instances = [
      {
        site = "ls";
        net = "wg";
      }
    ];
  };

  #services.meta-dhcpd = {
  #  enable = true;
  #  interfaces = [
  #    priIf.ifname
  #    secIf.ifname
  #  ];
  #  site = "home";
  #  nets = [ "lan" "wifi" "slowfi" "nanolan" ];
  #};

  services.meta-unbound = {
    enable = true;
    interfaces = [
      priIf.ifname
      #secIf.ifname
    ];
    site = "ls";
    nets = [ "k21" "wifi" "slowfi" "nanolan" ];
  };


  systemd.services.wpa_supplicant.enable = false;
  #services.hostapd = {
  #  enable = true;
  #  interface = secIf.ifname;
  #  ssid = secNet.cfg.ssid;
  #  wpaPassphrase = secNet.cfg.psk;
  #};

  networking = {
    dhcpcd.enable = false;
    useDHCP = false;

    defaultGateway = priNet.gateway;
    #nameservers = [ ] ++ priNet.dnsServers;
    #nameservers = [ "1.1.1.1" "1.0.0.1" ];
    nameservers = [ "192.168.0.6" "1.0.0.1" ];

    #wireless.interfaces = [ secIf.ifname ];
    #bridges = { br0 = { interfaces = [ m.rpi.home.lan.ifname ]; }; };


    interfaces = {
      "wg-ls-wg".mtu = 1390;
      } //
      (staticIf { iFace = priIf; network = priNet; });# //
      #(staticIf { iFace = wifiIf; network = wifiNet; }) //
      #(staticIf { iFace = secIf; network = secNet; });
      #br0 = {
      #  ipv4 = {
      #    addresses = [
      #      {"address" = machines.rpi.lan.ip; "prefixLength" = 24;}
      #    ];
      #    routes = [
      #      { address = machines.network.lan.subnet; prefixLength = 24; via = machines.network.lan.gateway; }
      #    ];
      #  };
      #};

    #nat = {
    #  enable = true;
    #  #externalInterface = machines.rpi.brname;
    #  externalInterface = priIf.ifname;
    #  externalIP = priIf.ip;
    #  internalInterfaces = [ secIf.ifname ];
    #  internalIPs = [ secIf.ip ];
    #};

    firewall.enable = false;
  };
}
