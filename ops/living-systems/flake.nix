{
  description = "living-systems NixOps";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    mailserver  = { url = "gitlab:simple-nixos-mailserver/nixos-mailserver?rev=f535d8123c4761b2ed8138f3d202ea710a334a1d"; };
    #storage     = { url = "gitlab:myrealm/storage/main"; };
    storage = { url = "git+ssh://gitea@np.lan/living-systems/storage?ref=main"; };
    profiles    = { url = "gitlab:myrealm/profiles/main"; };
    layers      = { url = "gitlab:myrealm/layers/main"; };
    nanopi      = { url = "gitlab:myrealm/nanopi-r4s/main"; };
    myrealm     = { url = "gitlab:myrealm/packages/main"; };
    sops-nix    = { url = "github:Mic92/sops-nix"; };
    #nixpkgs     = { url = "github:NixOS/nixpkgs/nixos-23.05"; };
    nixpkgs     = { url = "github:NixOS/nixpkgs/nixpkgs-unstable"; };
    #rpi         = { url = "github:leonardp/raspberry-pi-nix"; };
    rpi         = { url = "github:tstat/raspberry-pi-nix"; };
  };

  outputs = { self, nixpkgs, storage, profiles, layers, nanopi, myrealm, rpi, ... }@inputs:
  let
    networks = storage.nixosModules.networks;
    machines = storage.nixosModules.machines;
    connectivity = storage.nixosModules.connectivity;

    pmod = profiles.nixosModules.default;
    pmods = profiles.nixosModules;
    lmod = layers.nixosModules.default;
    lmods = layers.nixosModules;

    defaultImports = [
      ({ config, pkgs, ... }: { nixpkgs.overlays = [ myrealm.overlays.default ]; })
      pmod
      lmod
      inputs.sops-nix.nixosModules.sops
    ];

    raspi = defaultImports ++ [
      lmods.meta-dhcpd
      lmods.meta-unbound
      lmods.meta-wireguard
      #inputs.rpi.rpi-3b-plus
      inputs.rpi.nixosModules.raspberry-pi
      #lmods.phppgadmin
      {
        hardware = {
          bluetooth.enable = true;

          raspberry-pi = {
            i2c.enable = true;

            config = {

              all = {
                base-dt-params = {
                  # enable autoprobing of bluetooth driver
                  # https://github.com/raspberrypi/linux/blob/c8c99191e1419062ac8b668956d19e788865912a/arch/arm/boot/dts/overlays/README#L222-L224
                  krnbt = {
                    enable = true;
                    value = "on";
                  };
                  #audio = {
                  #  enable = true;
                  #  value = "off";
                  #};
                };
                dt-overlays = {
                  vc4-kms-v3d = {
                    enable = false;
                    params = {
                      #audio = {
                      #  enable = true;
                      #  value = "off";
                      #};
                    };
                  };
                  hifiberry-dacplus = {
                    enable = true;
                    params = { };
                  };
                };

              };
            };

          };
        };
      }
    ];
  in
  {
    nixosConfigurations.rpi = nixpkgs.lib.nixosSystem {
      specialArgs = {
        inherit machines connectivity networks;
      };
      modules = raspi ++ [
        #"${nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64.nix"
        #"${nixpkgs}/nixos/modules/installer/sd-card/sd-image-raspberrypi.nix"
      ];
    };

    images.rpi = self.nixosConfigurations.rpi.config.system.build.sdImage;

    nixopsConfigurations.default = {
      #enableRollback = true;
      #storage.memory = {}; # fire and forget
      network.storage.legacy = {
        databasefile = "~/.nixops/deployments.nixops";
      }; # save to ~/.nixops
      #nixpkgs.pkgs = pkgsFor "x86_64-linux";
      # TODO fixme
      nixpkgs = nixpkgs;
      defaults._module.args = {
        inherit machines connectivity networks;
      };

      proxy =  {
        deployment = {
          #targetHost = "proxy.living-systems.dev";
          targetHost = "116.203.106.198";
          provisionSSHKey = false;
        };

        imports = defaultImports ++ [
          inputs.mailserver.nixosModule
          lmods.meta-wg-server
          ./proxy
        ];
      };

      nanopi =  {
        deployment = {
          targetHost = "192.168.0.7";
          #targetHost = "10.100.0.3";
          provisionSSHKey = false;
        };

        imports = defaultImports ++ [
          ./nanopi
          lmods.phppgadmin
          lmods.meta-dhcpd
          lmods.meta-unbound
          lmods.meta-wireguard
          nanopi.nixosModules.default
        ];
      };

      rpi =  {
        deployment = {
          targetHost = "192.168.0.6";
          #targetHost = "192.168.0.208";
          provisionSSHKey = false;
        };
        imports = raspi ++ [
          ./rpi
          #lmods.phppgadmin
          #nanopi.nixosModules.default
        ];
      };
    };
  };
}
