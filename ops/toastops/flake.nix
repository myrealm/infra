{
  description = "NixOps test flake";

  inputs = {
    #storage     = { url = "gitlab:myrealm/storage/main"; };
    storage = { url = "git+ssh://gitea@np.lan/living-systems/storage?ref=main"; };
    profiles  = { url = "gitlab:myrealm/profiles/main"; };
    layers    = { url = "gitlab:myrealm/layers/main"; };
    myrealm   = { url = "gitlab:myrealm/packages/main"; };
  };

  outputs = { self, nixpkgs, ... }@inputs:
  let
    pmod = inputs.profiles.nixosModules.default;
    lmod = inputs.layers.nixosModules.default;
    defaultImports = [
      ({ config, pkgs, ... }: { nixpkgs.overlays = [ inputs.myrealm.overlays.default ]; })
      pmod
      lmod
    ];
    networks = inputs.storage.nixosModules.networks;
    machines = inputs.storage.nixosModules.machines;
  in
  {
    nixopsConfigurations.default = {
      #enableRollback = true;
      #storage.memory = {}; # fire and forget
      network.storage.legacy = {
        databasefile = "~/.nixops/flaaaaaaaaake.nixops";
      }; # save to ~/.nixops
      #nixpkgs.pkgs = pkgsFor "x86_64-linux";
      # TODO fixme
      nixpkgs = nixpkgs;
      defaults._module.args = {
        #  inherit inputs self;
        inherit machines networks;
      };

      toast =  {
        imports = defaultImports ++ [
          ./toastops.nix
          ./test.nix
        ];
        deployment = {
          targetHost = "192.168.0.184";
          provisionSSHKey = false;
        };
      };
    };
  };
}
