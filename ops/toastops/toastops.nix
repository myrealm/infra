{ config, pkgs, ... }:
{
  networking.hostName = "toastops";

  hardware.enableRedistributableFirmware = true;

  # Select internationalisation properties.
  console.keyMap = "de";
  time.timeZone = "Europe/Amsterdam";

  myrealm.profile.enable = true;
  myrealm.profile.hardware = "qemu-efi";
  myrealm.profile.gui = "headless";
  myrealm.profile.service = "client-dhcp";

  #myrealm.profile.resizefs = true; # working!

  #myrealm.layer.enable = true;
  #myrealm.layer.users = true;
  #myrealm.layer.machine = "toastops";

  #myrealm.layer.domain.ls = {
  #  enable = true;

  #  globals = {
  #    build-host.enable = true;
  #  };

  #  storage = {
  #    secret = "/root/secrets";
  #    www = "/www";
  #    mut = "/data";
  #    cert = "/www/acme";
  #  };
  #};
}
