{ config, pkgs, machines, ... }:
{
  imports = [
    #../../modules/docker.nix
    #../../../containers/yii
    #../../../modules/lxd.nix
    #../../profiles/packages/lls1-test.nix
    #./steam.nix

    ./hardware-configuration.nix

    ./rsnapshot.nix
    ./exports.nix
    ./udev-rules.nix
    ./libvirt.nix

    ./nginx-epd.nix
    ./phpdev.nix
    #./yii-advanced.nix
    #./postgresql.nix

    ./network.nix

    ./binary-cache.nix
    ./myrealm.nix
    ./sops.nix
    ./samba.nix
  ];

  #nixpkgs.config.allowBroken = true;
  programs.bcc.enable = true;

  #system.autoUpgrade = {
  #  enable = true;
  #  dates = "9:00";
  #};
  #nix = {
  #  # disabled to keep aarch64 packages
  #  #gc = {
  #  #  automatic = true;
  #  #  dates = "21:00";
  #  #};
  #};

  virtualisation.docker.enable = true;
  #users.users.leo.extraGroups = [ "docker" ];
  programs.direnv.enable = true;

}
