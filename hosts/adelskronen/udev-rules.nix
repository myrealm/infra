{ config, ... }:
{
  services.udev.extraRules = ''
    # Seedstudio XIAO
    ATTRS{idVendor}=="2886" ATTRS{idProduct}=="002f", ENV{ID_MM_TTY_BLACKLIST}="1"
    # STM dfu bootloader
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="0666"
    # ARM DAPLink CMSIS-DAP -- reel-board
    ATTR{idProduct}=="0204", ATTR{idVendor}=="0d28", MODE="0666", GROUP="users"
    # AZDelivery Logic Anal
    ATTR{idProduct}=="3881", ATTR{idVendor}=="0925", MODE="0666", GROUP="users"
  '';
}
