{ pkgs, config, lib, ...}:
let
  appname = "phpdev";
  baseDir = "/opt/${appname}";
  mysqlDir = "/opt/mysql";
  logDir = "${baseDir}/logs";
  phpLogDir = "${baseDir}/logs/php";
  nginxLogDir = "${baseDir}/logs/nginx";
  nginxDataDir = "${baseDir}/htdocs";
  appTables = ''
    CREATE DATABASE IF NOT EXISTS `inventory`;

    USE `inventory`;

    CREATE TABLE `labels_templates` (
      template_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      description VARCHAR(255) NOT NULL,
      template TEXT NOT NULL,
      table_name VARCHAR(255) NOT NULL
    );

    CREATE TABLE `labels_label_1` (
      label_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      template_id INT NOT NULL,
      CONSTRAINT `fk_template_label1`
        FOREIGN KEY (template_id) REFERENCES labels_templates (template_id)
        ON UPDATE RESTRICT,
      line1 VARCHAR(20) DEFAULT "",
      line2 VARCHAR(20) DEFAULT "",
      line3 VARCHAR(20) DEFAULT ""
    );

    CREATE TABLE IF NOT EXISTS `items` (
      `inventory_id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      template_id INT NOT NULL,
      CONSTRAINT `fk_template_items`
        FOREIGN KEY (template_id) REFERENCES labels_templates (template_id)
        ON UPDATE RESTRICT,
      `description` varchar(255) DEFAULT NULL,
      `serial_number` varchar(255) DEFAULT NULL,
      `person_responsible` varchar(255) NOT NULL
    );

    CREATE TABLE IF NOT EXISTS `obscure_hashes` (
      `hash` CHAR(32) NOT NULL PRIMARY KEY,
      `inventory_id` INT NOT NULL
    );

    CREATE TRIGGER add_hash
    AFTER INSERT ON `items`
    FOR EACH ROW
    INSERT INTO `obscure_hashes` (`hash`, `inventory_id`) VALUES (MD5(CONCAT('$salt$', NEW.inventory_id)), NEW.inventory_id);

    CREATE TRIGGER delete_hash
    AFTER DELETE ON `items`
    FOR EACH ROW
    DELETE FROM `obscure_hashes` WHERE `inventory_id`=OLD.inventory_id;

    INSERT INTO `labels_templates` (`template_id`, `description`, `template`, `table_name`) VALUES (NULL, "Test Template", "$str0, $str1, $str2", "test");
  '';
in
{
  systemd.tmpfiles.rules = [
    "d ${baseDir} 1770 nginx ${appname}"
    "d ${logDir} 1770 nginx ${appname}"
    "d ${nginxLogDir} 1770 nginx ${appname}"
    "d ${phpLogDir} 1770 ${appname} ${appname}"
    "d ${nginxDataDir} 1770 nginx ${appname}"
    #"d ${mysqlDir} 1700 mysql mysql"
    #"z ${mysqlDir} 1700 mysql mysql"
  ];

  networking.extraHosts = "127.0.0.1 ${appname}.local";

  environment.systemPackages = with pkgs; [
    php
    #phpExtensions.zmq # marked broken
    phpPackages.composer
  ];

  users.users.${appname} = {
    isSystemUser = true;
    #createHome = true;
    #home = baseDir;
    group  = appname;
  };
  users.groups.${appname} = {
    members = [ "leo" ];
  };


  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    dataDir = mysqlDir;
    bind = "0.0.0.0";
    port = 3306;
    settings = {
        mysqld = {
          key_buffer_size = "6G";
          table_cache = 1600;
          log-error = "/opt/mysql/mysql_err.log";
          #plugin-load-add = [ "server_audit" "ed25519=auth_ed25519" ];
          plugin-load-add = [ "server_audit" ];
      };
    };
    ensureUsers = [
      {
        name = appname;
        ensurePermissions = {
          "*.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "leo";
        ensurePermissions = {
          "*.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "backup";
        ensurePermissions = {
          "*.*" = "SELECT, LOCK TABLES";
        };
      }
    ];
    ensureDatabases = [
      "inventory"
    ];
    initialScript = pkgs.writeText "iinit.sql" appTables;
  };

  systemd.services.nginx.serviceConfig.ReadWritePaths = [ nginxLogDir ];
  services.nginx = {
    enable = true;
    virtualHosts."${appname}.local" = {
      root = "${nginxDataDir}/hardware/web";
      listen = [ { addr = "0.0.0.0"; port = 4321; } ];
      serverAliases = [ "adelskronen.lan" "adelskronen" "192.168.0.10" ];
      extraConfig = ''
        error_log ${nginxLogDir}/error.log;
        access_log ${nginxLogDir}/access.log;
      '';
      locations = {
        "/" = {
          extraConfig = ''
            index index.php index.html;
            try_files $uri $uri/ /index.php$is_args$args;
          '';
        };
        "~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$" = {
          tryFiles = "$uri =404";
        };
        #"~ ^/assets/.*\.php$" = {
        #  extraConfig = ''
        #    deny all;
        #  '';
        #};
        "~ \.php" = {
          extraConfig = ''
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass unix:${config.services.phpfpm.pools.${appname}.socket};
            try_files $uri =404;
          '';
        };
        #"~* /\." = {
        #  extraConfig = ''
        #    deny all;
        #    # access_log off;
        #    # log_not_found off;
        #  '';
        #};
      };
    };
  };

  services.phpfpm = {
      # marked broken :(
      # extension=${pkgs.phpExtensions.zmq}/lib/php/extensions/zmq.so
    phpOptions = ''
      cgi.fix_pathinfo=0
    '';
    pools.${appname} = {
      user = appname;
      settings = {
        "listen.owner" = config.services.nginx.user;
        "pm" = "dynamic";
        "pm.max_children" = 32;
        "pm.max_requests" = 500;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 5;
        "php_admin_value[error_log]" = "stderr";
        "php_admin_flag[log_errors]" = true;
        "catch_workers_output" = true;
      };
      phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
    };
  };
}
