{ config, machines, networks, ... }:
let
  m = machines;
  n = networks;
in
{
  services.nfs.server = {
    enable = true;
    exports = ''
      /export ${m.heineken.home.wifi.ip}(fsid=0,no_subtree_check)
      /export/data ${m.heineken.home.wifi.ip}(rw,all_squash,sync,no_subtree_check,nohide,anonuid=1000,anongid=65534,insecure)
    '';
  };
}
