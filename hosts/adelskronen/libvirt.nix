{ config, pkgs, lib, ... }:
let
  cfg =  {
    portMap = {
      "5959" = 5900;
    };
    sslCert = "/opt/myrealm/ls/acme/cert.pem";
    sslKey = "/opt/myrealm/ls/acme/key.pem";
  };
in
{
  environment.systemPackages = with pkgs; [
    virtiofsd
  ];

  virtualisation.libvirtd = {
    enable = true;
    # qemuPackage = pkgs.qemu_kvm;
  };
  virtualisation.spiceUSBRedirection.enable = true;

  # add optional network config here?
  # networking.bridges.lxdbr0 = {
  #   interfaces = [ ];
  # };
  #
  #networking.iproute2.rttablesExtraConfig = "
  #  ip -4 route add 192.168.0.0/24 via 192.168.0.1
  #";

  # something strange...
  security.apparmor.enable = false;

  services.networking.websockify = {
    enable = false;
    sslCert = "/opt/myrealm/ls/acme/cert.pem";
    sslKey = "/opt/myrealm/ls/acme/key.pem";
    portMap = {
      "5959" = 5900;
    };
  };

  systemd.services."websockify@" = {
    description = "Service to forward websocket connections to TCP connections (from port:to port %I)";
    script = ''
      IFS=':' read -a array <<< "$1"
      ${pkgs.python3Packages.websockify}/bin/websockify --ssl-only \
        --cert=${cfg.sslCert} --key=${cfg.sslKey} 0.0.0.0:''${array[0]} 0.0.0.0:''${array[1]}
    '';
    scriptArgs = "%i";
  };

  systemd.targets.default-websockify = {
    description = "Target to start all default websockify@ services";
    unitConfig.X-StopOnReconfiguration = true;
    wants = lib.mapAttrsToList (name: value: "websockify@${name}:${toString value}.service") cfg.portMap;
    wantedBy = [ "multi-user.target" ];
  };

  networking.firewall.allowedTCPPorts = [
    4500
    5959
    5900
  ];
  networking.extraHosts = ''
    127.0.0.1 adelskronen.living-systems.dev
  '';

  services.nginx = {
    enable = true;
    virtualHosts."adelskronen.living-systems.dev" = {
      forceSSL = true;
      root = "/opt/libvirt/www/spice";
      locations."/" = {
        index = "index.html index.htm";
      };
      locations."/websockify/" = {
        proxyWebsockets = true;
        proxyPass = "https://127.0.0.1:5959";
        extraConfig = ''
          proxy_read_timeout 61s;
          proxy_buffering off;
        '';
      };
      sslCertificate = "/opt/myrealm/ls/acme/cert.pem";
      sslCertificateKey = "/opt/myrealm/ls/acme/key.pem";
      listen = [ { addr = "*"; port = 4500; ssl = true; } ];
    };
  };
}
