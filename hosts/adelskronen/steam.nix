{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    steam
  ];
  # make steam work
  programs.steam.enable = true;

  # Firewall Rules for A mongus
  networking.firewall.allowedTCPPortRanges = [ { from = 27015; to = 27030;} { from = 27036; to = 27037; } ];
  networking.firewall.allowedUDPPorts = [ 4380 27036 ];
  networking.firewall.allowedUDPPortRanges = [ { from = 27000; to = 27031; } ];
}
