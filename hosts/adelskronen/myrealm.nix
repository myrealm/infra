{
  myrealm.profile.enable = true;
  myrealm.profile.hardware = "desktop-bios";
  myrealm.profile.gui = "chonker";
  myrealm.profile.service = "ssh-only";

  # -> move to layer.domain.ls.metrics!
  #myrealm.profile.node-exporter = true;

  myrealm.layer.enable = true;
  myrealm.layer.users = true;
  myrealm.layer.machine = "adelskronen"; # mapping via layers to storage
  # ever useful?
  #myrealm.layer.paths = [
  #  ["ls" "k21"]
  #];

  myrealm.layer.domain.ls = {
    enable = true;
    #service.frontpage = {
    #  enable = true;
    #  ip = "0.0.0.0";
    #};
    #service.acme.enable = true;
    storage = {
      secret = "/root/secrets";
      mut = "/opt/myrealm/ls";
      cert = "/opt/myrealm/acme";
      www = "/opt/myrealm/www";
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults = { server = "https://acme-v02.api.letsencrypt.org/directory"; };
    #defaults = { server = "https://acme-staging-v02.api.letsencrypt.org/directory"; };

    certs = let ls = "living-systems.dev"; in {
      "${ls}" = {
        dnsProvider = "hetzner";
        credentialsFile = "/root/secrets/hetzner/acme-dns";

        email = "admin@${ls}";
        extraDomainNames = [
          "*.${ls}"
          #"cloud.${ls}"
        ];
      };
    };
  };
}
