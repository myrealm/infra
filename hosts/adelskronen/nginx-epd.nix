{ config, ... }:
{
  services.nginx = {
    enable = true;
    virtualHosts."fractalbmp" = {
      listen = [{ addr = "0.0.0.0"; port = 500; }];
      root = "/opt/nginx-static";
    };
  };
}
