{ config, pkgs, lib, ... }:
let
  storageDir = "/www";

  logFmt = ''
    log_format json_analytics escape=json '{'
                      '"msec": "$msec", ' # request unixtime in seconds with a milliseconds resolution
                      '"connection": "$connection", ' # connection serial number
                      '"connection_requests": "$connection_requests", ' # number of requests made in connection
                      '"pid": "$pid", ' # process pid
                      '"request_id": "$request_id", ' # the unique request id
                      '"request_length": "$request_length", ' # request length (including headers and body)
                      '"remote_addr": "$remote_addr", ' # client IP
                      '"remote_user": "$remote_user", ' # client HTTP username
                      '"remote_port": "$remote_port", ' # client port
                      '"time_local": "$time_local", '
                      '"time_iso8601": "$time_iso8601", ' # local time in the ISO 8601 standard format
                      '"request": "$request", ' # full path no arguments if the request
                      '"request_uri": "$request_uri", ' # full path and arguments if the request
                      '"args": "$args", ' # args
                      '"status": "$status", ' # response status code
                      '"body_bytes_sent": "$body_bytes_sent", ' # the number of body bytes exclude headers sent to a client
                      '"bytes_sent": "$bytes_sent", ' # the number of bytes sent to a client
                      '"http_referer": "$http_referer", ' # HTTP referer
                      '"http_user_agent": "$http_user_agent", ' # user agent
                      '"http_x_forwarded_for": "$http_x_forwarded_for", ' # http_x_forwarded_for
                      '"http_host": "$http_host", ' # the request Host: header
                      '"server_name": "$server_name", ' # the name of the vhost serving the request
                      '"request_time": "$request_time", ' # request processing time in seconds with msec resolution
                      '"upstream": "$upstream_addr", ' # upstream backend server for proxied requests
                      '"upstream_connect_time": "$upstream_connect_time", ' # upstream handshake time incl. TLS
                      '"upstream_header_time": "$upstream_header_time", ' # time spent receiving upstream headers
                      '"upstream_response_time": "$upstream_response_time", ' # time spend receiving upstream body
                      '"upstream_response_length": "$upstream_response_length", ' # upstream response length
                      '"upstream_cache_status": "$upstream_cache_status", ' # cache HIT/MISS where applicable
                      '"ssl_protocol": "$ssl_protocol", ' # TLS protocol
                      '"ssl_cipher": "$ssl_cipher", ' # TLS cipher
                      '"scheme": "$scheme", ' # http or https
                      '"request_method": "$request_method", ' # request method
                      '"server_protocol": "$server_protocol", ' # request protocol, like HTTP/1.1 or HTTP/2.0
                      '"pipe": "$pipe", ' # "p" if request was pipelined, "." otherwise
                      '"gzip_ratio": "$gzip_ratio", '
                      '"http_cf_ray": "$http_cf_ray",'
                      '"geoip_country_code": "$geoip_country_code"'
                      '}';

    access_log /var/log/nginx/json_access.log json_analytics;
  '';

  geoIp = ''
    geoip_country ${pkgs.geolite-legacy}/GeoIP.dat;
    geoip_city ${pkgs.geolite-legacy}/GeoLiteCity6.dat;
  '';
in
{
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  myrealm.layer.domain.ls.globals.promtail.scrapeConfigs = [ # ok.
''
- job_name: system
  pipeline_stages:
  - replace:
      expression: '(?:[0-9]{1,3}\.){3}([0-9]{1,3})'
      replace: '***'
  static_configs:
  - targets:
     - localhost
    labels:
     job: nginx_access_log
     host: proxy
     agent: promtail
     __path__: /var/log/nginx/json_access.log
''
  ];

  users.users.nginxtest = {
    group = "nginxtest";
    isSystemUser = true;
  };
  users.groups.nginxtest.members = [ "nginxtest" config.services.nginx.user ];

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;

    # !!! default
    virtualHosts."living-systems.dev" = {
      # !!!
      default = true;
      # !!!
      forceSSL = true;
      sslCertificate = "${storageDir}/acme/living-systems.dev/fullchain.pem";
      sslCertificateKey = "${storageDir}/acme/living-systems.dev/key.pem";

      #listen = [ { addr = "10.100.0.1"; port = 9101; } ];
      extraConfig = logFmt;

      root = "${storageDir}/portal";
      locations = {
        "/" = {
          tryFiles = "$uri /index.php";
        };

        "~ \.php$" = {
          tryFiles = "$uri =404";
          extraConfig = ''
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:${config.services.phpfpm.pools.nginxtest.socket};
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
          '';
        };
      };
    };

    virtualHosts."portal.living-systems.dev" = {
      forceSSL = true;
      sslCertificate = "${storageDir}/acme/living-systems.dev/fullchain.pem";
      sslCertificateKey = "${storageDir}/acme/living-systems.dev/key.pem";
      locations = {
        "/" = {
          proxyPass = "http://10.100.0.3:80";
        };
      };
    };

    virtualHosts."metrics" = {
      listen = [ { addr = "10.100.0.1"; port = 9101; } ];
      locations = {
        "/metrics" = {
          extraConfig = ''
	    stub_status on;
          '';
        };
      };
    };
  };

  services.phpfpm.pools.nginxtest = {
    user = "nginxtest";
    group = "nginxtest";
    phpPackage = pkgs.php;

    settings = {
      "listen.owner" = config.services.nginx.user;
      "pm" = "dynamic";
      "pm.max_children" = 16;
      "pm.max_requests" = 100;
      "pm.start_servers" = 2;
      "pm.min_spare_servers" = 2;
      "pm.max_spare_servers" = 4;
      "php_admin_value[error_log]" = "stderr";
      "php_admin_flag[log_errors]" = true;
      "catch_workers_output" = true;
    };

    phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
  };
}
