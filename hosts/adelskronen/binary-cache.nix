{ config, machines, networks, ... }:
let
  m = machines;
  n = networks;
in
{
  # enable http api
  services.nix-serve = {
    enable = true;
    #secretKeyFile = "/etc/nixos/secrets/cache-priv-key.pem";
    secretKeyFile = "/opt/cache-priv-key.pem";
  };

  # keep build-time dependencies
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';

  # nginx proxy
  services.nginx = {
    enable = true;
    virtualHosts = {
      # ... existing hosts config etc. ...
      "${m.adelskronen.home.lan.name}.${n.home.lan.domain}" = {
        serverAliases = [ "binarycache" ];
        locations."/".extraConfig = ''
          proxy_pass http://localhost:${toString config.services.nix-serve.port};
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        '';
      };
    };
  };
}
