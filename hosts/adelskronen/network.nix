{ config, machines, networks, connectivity, lib, ... }:
let
  m = machines;
  n = networks;
  name = "adelskronen";
  priIf = m.${name}.ls.k21;
  priNet = n.ls.k21;
in
{
  networking.hostName = name;

  # required for ZFS -> `head -c 8 /etc/machine-id`
  networking.hostId = "307e7c9a";

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
    "net.ipv6.conf.all.forwarding" = true;
  };

  services.meta-wireguard = {
    enable = true;
    hostName = name;
    instances = [
      #{
      #  site = "sofia";
      #  net = "wg";
      #}
      #{
      #  site = "tsti";
      #  net = "wg";
      #}
      {
        site = "ls";
        net = "wg";
      }
    ];
  };

  services.meta-network = {
    enable = true;
  };

  # thank's steven
#  networking.extraHosts =
#    let
#      hostsPath = https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts;
#      hostsFile = builtins.fetchurl hostsPath;
#    in
#    builtins.readFile "${hostsFile}";
#
  #services.resolved.enable = lib.mkForce false;

  networking = {
    dhcpcd.enable = false;

    #search = [ "${net.home.lan.domain}" ];
    #search = [ "ipc.uni-tuebingen.de" "lan" ];
    search = [ "lan" ];

    defaultGateway = priNet.gateway;
    #nameservers = [ ] ++ priNet.dnsServers;
    #nameservers = [ "192.168.0.6" ];
    #nameservers = [ "1.1.1.1" ];
    nameservers = [ "192.168.0.6" "1.1.1.1" ];

    #macvlans.wired = {
    #  mode = "bridge";
    #  interface = "enp39s0";
    #};
    #interfaces.wired = {
    #  useDHCP = true;
    #};

    networkmanager.unmanaged = [
      "enp39s0"
    ];
    macvlans.macvtap0 = {
      interface = "enp39s0";
      mode = "bridge";
    };
    interfaces.macvtap0 = {
      macAddress = "52:54:00:f3:5b:12";
    #interfaces.enp39s0 = {
      mtu = 1460;
      useDHCP = false;
      ipv4 = {
        addresses = [
          {
            address = priIf.ip;
            prefixLength = priNet.length;
          }
        ];
        routes = [
          {
            address = priNet.prefix;
            prefixLength = priNet.length;
            via = priNet.gateway;
          }
        ];
      };
    };
  };

  networking.firewall.enable = true;
  #networking.firewall.checkReversePath = false;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22    # ssh
    80    # 80 nginx proxy for binary cache
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5432  # phppgadmin
    5555  # ggm-http
    8000  # http-static
    49152 # zpinger
    34057 # zyre testapp
    20048 # zyre testapp
    42639 # zyre testapp
  ];

  networking.firewall.allowedUDPPorts = [
    123   # NTP
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5670  # ZRE-DISC
    43090 # zyre testapp
    59607 # zyre testapp
  ];

  # gilgamesh port range
  networking.firewall.allowedTCPPortRanges = [
    { from = 6000; to = 6010; }
    { from = 1714; to = 1764; }
  ];
}
