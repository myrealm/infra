{ config, lib, pkgs, ... }:

{
  hardware.enableRedistributableFirmware = true;

  boot.loader.grub.enable = true;
  boot.loader.grub.memtest86.enable = true;
  boot.loader.timeout = 5;
  boot.loader.grub.device = "/dev/nvme0n1";

  #boot.kernelPackages = pkgs.linuxPackages_5_4;
  #boot.kernelPackages = pkgs.linuxPackages;
  #boot.kernelPackages = pkgs.unstable.linuxPackages_5_10;
  #boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  #boot.zfs.enableUnstable = true;
  #boot.kernelPackages = pkgs.linuxPackages;

  boot.initrd.availableKernelModules = [ "nvme" "ahci" "xhci_pci" "usbhid" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  boot.kernelParams = [
    "acpi_enforce_resources=lax"
    "zfs.zfs_arc_max=8589934592"
  ];
  boot.extraModprobeConfig = "options iwlwifi 11n_disable=1"; # whyyy?!?!
  boot.supportedFilesystems = [ "zfs" ];

  # mount tmpfs on /tmp -> not good for build hosts
  boot.tmp.useTmpfs = lib.mkOverride 100 false;

  # emulate arm
  boot.binfmt.emulatedSystems = [ "aarch64-linux" "armv7l-linux" ];

  hardware.opengl = {
    enable = true;
    setLdLibraryPath = true;
  };
  # nonfree nvidia drivers
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.powerManagement.enable = true; # maybe?!

  # default soundcard is not hdmi
  sound.extraConfig = ''
    pcm.!default {
        type hw
        card 1
    }

    ctl.!default {
        type hw
        card 1
    }
  '';

  hardware.firmware = [ pkgs.firmwareLinuxNonfree ];

  environment.systemPackages = with pkgs; [
    pkgs.firmwareLinuxNonfree
  ];

  # keymaps
  console = { keyMap = "de"; };
  services.xserver.layout = "de";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/6133ea81-7374-4283-a4cf-66d222f451bb";
    fsType = "ext4";
  };

  fileSystems."/opt" = {
    device = "/dev/disk/by-uuid/a43a2ae7-6293-477f-af2c-3488bb2c9ebf";
    fsType = "ext4";
    options = [ "user" "users" "exec" ];
  };

  fileSystems."/tank" = {
    device = "tank/data";
    fsType = "zfs";
    options = [ "user" "users" ];
  };

  # bind mounts
  # everything should be accessed through bind mounts where possible
  fileSystems."/mnt/data" = {
    device = "/tank";
    options = [ "bind" ];
  };

  fileSystems."/export/data" = {
    device = "/mnt/data";
    options = [ "bind" ];
  };

  #swapDevices = [ { device = "/opt/swapfile"; size=32768; } ];

  nix.settings.max-jobs = lib.mkDefault 16;
}
