{ pkgs, config, lib, ...}:
let
  appname = "grav";
  baseDir = "/opt/${appname}";
  logDir = "${baseDir}/logs";
  phpLogDir = "${baseDir}/logs/php";
  nginxLogDir = "${baseDir}/logs/nginx";
  nginxDataDir = "${baseDir}/htdocs";
in
{
  systemd.tmpfiles.rules = [
    "d ${baseDir} 1770 nginx ${appname}"
    "d ${logDir} 1770 nginx ${appname}"
    "d ${nginxLogDir} 1770 nginx ${appname}"
    "d ${phpLogDir} 1770 ${appname} ${appname}"
    "d ${nginxDataDir} 1770 nginx ${appname}"
  ];

  networking.extraHosts = "127.0.0.1 ${appname}.local";

  users.users.${appname} = {
    isSystemUser = true;
    #createHome = true;
    #home = baseDir;
    group  = appname;
  };
  users.groups.${appname} = {
    members = [ "leo" ];
  };

  systemd.services.nginx.serviceConfig.ReadWritePaths = [ nginxLogDir ];
  services.nginx = {
    enable = true;
    virtualHosts."${appname}.local" = {
      root = "${nginxDataDir}";
      listen = [ { addr = "0.0.0.0"; port = 8080; } ];
      serverAliases = [ "adelskronen.lan" "adelskronen" "192.168.0.10" ];
      extraConfig = ''
        error_log ${nginxLogDir}/error.log;
        access_log ${nginxLogDir}/access.log;
      '';
      locations = {
        "/" = {
          extraConfig = ''
            index index.php index.html;
            try_files $uri $uri/ /index.php$is_args$args;
          '';
        };
        "~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$" = {
          tryFiles = "$uri =404";
        };
        #"~ ^/assets/.*\.php$" = {
        #  extraConfig = ''
        #    deny all;
        #  '';
        #};
        "~ \.php" = {
          extraConfig = ''
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass unix:${config.services.phpfpm.pools.${appname}.socket};
            try_files $uri =404;
          '';
        };
        #"~* /\." = {
        #  extraConfig = ''
        #    deny all;
        #    # access_log off;
        #    # log_not_found off;
        #  '';
        #};
      };
    };
  };

  services.phpfpm = {
      # marked broken :(
      # extension=${pkgs.phpExtensions.zmq}/lib/php/extensions/zmq.so
    phpOptions = ''
      cgi.fix_pathinfo=0
    '';
    pools.${appname} = {
      user = appname;
      settings = {
        "listen.owner" = config.services.nginx.user;
        "pm" = "dynamic";
        "pm.max_children" = 32;
        "pm.max_requests" = 500;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 5;
        "php_admin_value[error_log]" = "stderr";
        "php_admin_flag[log_errors]" = true;
        "catch_workers_output" = true;
      };
      phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
    };
  };
}
