{ pkgs, config, lib, ...}:
let
  appname = "yiiadv";
  baseDir = "/opt/${appname}";
  logDir = "${baseDir}/logs";
  phpLogDir = "${baseDir}/logs/php";
  nginxLogDir = "${baseDir}/logs/nginx";
  nginxDataDir = "${baseDir}/htdocs";
  mysqlDir = "/opt/mysql";
in
{
  systemd.tmpfiles.rules = [
    "d ${baseDir} 1770 nginx ${appname}"
    "d ${logDir} 1770 nginx ${appname}"
    "d ${nginxLogDir} 1770 nginx ${appname}"
    "d ${phpLogDir} 1770 ${appname} ${appname}"
    "d ${nginxDataDir} 1770 nginx ${appname}"
    "d ${mysqlDir} 1700 mysql mysql"
    "z ${mysqlDir} 1700 mysql mysql"
  ];

  networking.extraHosts = ''
    127.0.0.1 f.yii.local
    127.0.0.1 b.yii.local
    127.0.0.1 ${appname}-front.local
    127.0.0.1 ${appname}-back.local
  '';

  environment.systemPackages = with pkgs; [
    php
    #phpExtensions.zmq # marked broken
    phpPackages.composer
  ];

  users.users.${appname} = {
    isSystemUser = true;
    #createHome = true;
    #home = baseDir;
    group  = appname;
  };

  users.groups.${appname} = {
    members = [ "leo" "nginx" ];
  };

  systemd.services.nginx.serviceConfig.ReadWritePaths = [ nginxLogDir ];

  services.nginx = {
    enable = true;
    virtualHosts."f.yii.local" = {
      listen = [ { addr = "0.0.0.0"; port = 80; } ];
      locations = {
        "/" = {
          proxyPass = "http://${appname}-front.local:8081";
        };
      };
    };
    virtualHosts."b.yii.local" = {
      listen = [ { addr = "0.0.0.0"; port = 80; } ];
      locations = {
        "/" = {
          proxyPass = "http://${appname}-back.local:8082";
        };
      };
    };
    virtualHosts."${appname}-front.local" = {
      #root = "${nginxDataDir}/y2adv/office/web";
      root = "${nginxDataDir}/yii-office-app/office/web";
      listen = [ { addr = "0.0.0.0"; port = 8081; } ];
      serverAliases = [ "adelskronen.lan" "adelskronen" "192.168.0.10" ];
      extraConfig = ''
        error_log ${nginxLogDir}/error.log;
        access_log ${nginxLogDir}/access.log;
      '';
      locations = {
        "/" = {
          extraConfig = ''
            index index.php index.html;
            try_files $uri $uri/ /index.php$is_args$args;
          '';
        };
        "~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$" = {
          tryFiles = "$uri =404";
        };
        #"~ ^/assets/.*\.php$" = {
        #  extraConfig = ''
        #    deny all;
        #  '';
        #};
        "~ \.php" = {
          extraConfig = ''
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass unix:${config.services.phpfpm.pools.${appname}.socket};
            try_files $uri =404;
          '';
        };
        #"~* /\." = {
        #  extraConfig = ''
        #    deny all;
        #    # access_log off;
        #    # log_not_found off;
        #  '';
        #};
      };
    };
    virtualHosts."${appname}-back.local" = {
      #root = "${nginxDataDir}/y2adv/admin/web";
      root = "${nginxDataDir}/yii-office-app/admin/web";
      listen = [ { addr = "0.0.0.0"; port = 8082; } ];
      serverAliases = [ "adelskronen.lan" "adelskronen" "192.168.0.10" ];
      extraConfig = ''
        error_log ${nginxLogDir}/error.log;
        access_log ${nginxLogDir}/access.log;
      '';
      locations = {
        "/" = {
          extraConfig = ''
            index index.php index.html;
            try_files $uri $uri/ /index.php$is_args$args;
          '';
        };
        "~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$" = {
          tryFiles = "$uri =404";
        };
        #"~ ^/assets/.*\.php$" = {
        #  extraConfig = ''
        #    deny all;
        #  '';
        #};
        "~ \.php" = {
          extraConfig = ''
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include ${pkgs.nginx}/conf/fastcgi_params;
            include ${pkgs.nginx}/conf/fastcgi.conf;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass unix:${config.services.phpfpm.pools.${appname}.socket};
            try_files $uri =404;
          '';
        };
        #"~* /\." = {
        #  extraConfig = ''
        #    deny all;
        #    # access_log off;
        #    # log_not_found off;
        #  '';
        #};
      };
    };
  };

  services.phpfpm = {
      # marked broken :(
      # extension=${pkgs.phpExtensions.zmq}/lib/php/extensions/zmq.so
    phpOptions = ''
      cgi.fix_pathinfo=0
    '';
    pools.${appname} = {
      user = appname;
      settings = {
        "listen.owner" = config.services.nginx.user;
        "pm" = "dynamic";
        "pm.max_children" = 32;
        "pm.max_requests" = 500;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 5;
        "php_admin_value[error_log]" = "stderr";
        "php_admin_flag[log_errors]" = true;
        "catch_workers_output" = true;
      };
      phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
    };
  };

  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    dataDir = mysqlDir;
    settings = {
        mysqld = {
          key_buffer_size = "6G";
          table_cache = 1600;
          log-error = "/opt/mysql/mysql_err.log";
          #plugin-load-add = [ "server_audit" "ed25519=auth_ed25519" ];
          plugin-load-add = [ "server_audit" ];
      };
    };
    ensureUsers = [
      {
        name = appname;
        ensurePermissions = {
          "*.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "leo";
        ensurePermissions = {
          "*.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "backup";
        ensurePermissions = {
          "*.*" = "SELECT, LOCK TABLES";
        };
      }
    ];
    ensureDatabases = [
      "yii2advanced"
    ];
    #initialScript = pkgs.writeText "iinit.sql" appTables;
  };
}
