{ config, pkgs, lib, ... }:
{
  networking.hostName = "toast";

  hardware.enableRedistributableFirmware = true;

  console = { keyMap = "de"; };
  services.xserver.layout = "de";

  boot.tmp.useTmpfs = lib.mkOverride 100 false;

  myrealm.profile.enable = true;
  myrealm.profile.hardware = "qemu-efi";
  #myrealm.profile.gui = "headless";
  myrealm.profile.gui = "office";
  myrealm.profile.service = "client-dhcp";

  myrealm.layer.enable = true;
  myrealm.layer.users = true;
  myrealm.layer.machine = "toast";

  myrealm.layer.domain.ls.enable = false;
}
