{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    (python3.withPackages(ps: with ps; [
      requests
      pyzmq
      aiohttp
      psycopg2
      blessed
      aioconsole
      aiozyre
    ]))
  ];
}
