{ config, lib, pkgs, ... }:
let
  ggmImports = [
    ../overlays # needed?
    ./containers-common.nix
    ./ggm-common.nix
  ];
  commonCfg = {
    autoStart = false;
    #autoStart = true;
    privateNetwork = true;
    hostBridge = "br0";
    hostAddress = "192.168.100.1";
  };
in
{
  containers.database = commonCfg // {
    localAddress = "192.168.100.10/24";

    config = {
      imports = ggmImports ++ [
        ./timescaledb.nix
      ];
    };

    bindMounts = {
      pgdata = {
        hostPath = "/opt/container-postgres";
        isReadOnly = false;
        mountPoint = "/datadir";
      };
    };
  };

  containers.host1 = commonCfg // {
    localAddress = "192.168.100.11/24";
    config = {
      imports = ggmImports;
    };
  };

  containers.host2 = commonCfg // {
    localAddress = "192.168.100.12/24";
    config = {
      imports = ggmImports;
    };
  };
}
