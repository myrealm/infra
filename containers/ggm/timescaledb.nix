{ pkgs, config, lib, ...}:
{
  # bind mount on guest recommended
  services.postgresql.dataDir = "/datadir";

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_12;
    extraPlugins = [ pkgs.timescaledb ];
    settings = {
      shared_preload_libraries = "timescaledb";
    };
    # TODO howto password/secret management for production
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE ROLE gilgamesh WITH LOGIN PASSWORD 'gilgamesh' CREATEDB;
      CREATE DATABASE gilgamesh;
      GRANT ALL PRIVILEGES ON DATABASE gilgamesh TO gilgamesh;
      \connect gilgamesh
      CREATE EXTENSION timescaledb
    '';
    enableTCPIP = true;
    authentication = lib.mkForce ''
      local all all              peer
      host  all all 127.0.0.1/32 md5
      host  all all ::1/128      md5
      host  all all 0.0.0.0/0    md5
      host  all all ::/0         md5
    '';

  };
  users.users.postgres.initialPassword = "postgres";
}
