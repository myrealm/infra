{ config, lib, pkgs, ... }:
let
  appname = "phpdev";

  guests = import ../guests.nix;
  commonCfg = import ../common.nix;
in
{
  environment.systemPackages = with pkgs; [
    php
    #phpExtensions.zmq # marked broken
    phpPackages.composer
  ];

  users.groups.${appname} = {
    members = [ "leo" ];
  };

  networking.bridges.containerbr = {
        interfaces = [ "container-veth" ];
  };
  networking.interfaces = {
    container-veth = {
      virtual = true;
    };
    containerbr = {
      ipv4 = {
        addresses = [
          { address = "192.168.100.1"; prefixLength = 24; }
        ];
      };
    };
  };

  networking.extraHosts = "${guests.yii_webstack.ip} ${appname}.local";

  containers.${guests.yii_webstack.name} = commonCfg // {
    localAddress = "${guests.yii_webstack.ip}/24";

    config = {
      systemd.tmpfiles.rules = [
        "d /datadir/logs 1770 ${appname} nginx 10d"
        "d /datadir/logs/nginx 1770 nginx nginx 10d"
        "d /datadir/logs/phpfpm 1770 ${appname} ${appname} 10d"
        "d /datadir/htdocs 1770 nginx ${appname} 10d"
      ];
      systemd.services.nginx.serviceConfig.ReadWritePaths = [ "/datadir/logs/nginx/" ];

      services.nginx = {
        enable = true;
        virtualHosts.${appname} = {
          root = "/datadir/basic/web";
          extraConfig = ''
            error_log /datadir/logs/nginx/error.log;
            access_log /datadir/logs/nginx/access.log;
          '';
          locations = {
            "/" = {
              extraConfig = ''
                try_files $uri $uri/ /index.php$is_args$args;
              '';
            };
            "~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$" = {
              tryFiles = "$uri =404";
            };
            "~ \.php" = {
                #fastcgi_pass unix:${config.services.phpfpm.pools.${appname}.socket};
              extraConfig = ''
                include ${pkgs.nginx}/conf/fastcgi_params;
                include ${pkgs.nginx}/conf/fastcgi.conf;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                try_files $uri =404;
              '';
            };
            "~* /\." = {
              extraConfig = ''
                deny all;
                # access_log off;
                # log_not_found off;
              '';
            };
          };
        };
      };

      services.phpfpm = {
        #phpOptions = ''
        #  extension=${pkgs.phpPackages.zmq}/lib/php/extensions/zmq.so
        #'';
        pools.${appname} = {
          user = appname;
          settings = {
            "listen.owner" = config.services.nginx.user;
            "pm" = "dynamic";
            "pm.max_children" = 32;
            "pm.max_requests" = 500;
            "pm.start_servers" = 2;
            "pm.min_spare_servers" = 2;
            "pm.max_spare_servers" = 5;
            "php_admin_value[error_log]" = "stderr";
            "php_admin_flag[log_errors]" = true;
            "catch_workers_output" = true;
          };
          phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
        };
      };

      users.users.${appname} = {
        isSystemUser = true;
        createHome = true;
        home = "/datadir";
        group  = appname;
      };
      users.groups.${appname} = {};
    };

    bindMounts = {
      mysqldata = {
        hostPath = "/opt/container-data/${guests.yii_webstack.name}";
        isReadOnly = false;
        mountPoint = "/datadir";
      };
    };
  };

  containers.${guests.yii_mysql.name} = commonCfg // {
    localAddress = "${guests.yii_mysql.ip}/24";

    config = {
      services.mysql = {
        enable = true;
        package = pkgs.mariadb;
        dataDir = "/datadir";
        ensureUsers = [
          {
            name = "yii";
            ensurePermissions = {
              "yii.*" = "ALL PRIVILEGES";
            };
          }
          {
            name = "backup";
            ensurePermissions = {
              "*.*" = "SELECT, LOCK TABLES";
            };
          }
        ];
      };
    };

    bindMounts = {
      mysqldata = {
        hostPath = "/opt/container-data/${guests.yii_mysql.name}";
        isReadOnly = false;
        mountPoint = "/datadir";
      };
    };
  };
}
