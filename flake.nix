{
  description = "modules";

  inputs = {
    #sp.url = "gitlab:myrealm/packages/main";
    storage.url = "gitlab:myrealm/storage/main";
    profiles.url = "gitlab:myrealm/profiles/main";
    layers.url = "gitlab:myrealm/layers/main";
    nanopi.url = "gitlab:myrealm/nanopi-r4s/main";

    # FIXME TODO
    # HOWTO?! inherit pkgs+overlays from myrealm/packages
    # while keeping everything from nixpkgs cache...
    # FIXME TODO
    #pkgs = { url = "gitlab:myrealm/packages/main"; };
    myrealm = { url = "gitlab:myrealm/packages/main"; };

    sops-nix    = { url = "github:Mic92/sops-nix"; };
    #pkgs = { url = "gitlab:myrealm/packages/main"; };
    #nixpkgs     = { url = "github:NixOS/nixpkgs/nixos-23.05"; };
    nixpkgs     = { url = "github:NixOS/nixpkgs/nixpkgs-unstable"; };
  };

  outputs = { self, storage, nixpkgs, profiles, layers, nanopi, ... }@inputs:
  let
    #myp = inputs.myrealm.packages;
    #pkgs = import myp.default { overlays = [ myp.overlays.default ]; };
    #overlay-myrealm = final: prev: {
    #  myrealm = nixpkgs.overlays.default;
    #};
  in
  {
    nixosConfigurations =
      let
        networks = storage.nixosModules.networks;
        machines = storage.nixosModules.machines;
        connectivity = storage.nixosModules.connectivity;

        pmod = profiles.nixosModules.default;
        pmods = profiles.nixosModules;
        lmod = layers.nixosModules.default;
        lmods = layers.nixosModules;

        defaultImports = [
          ({ config, pkgs, ... }: { nixpkgs.overlays = [ inputs.myrealm.overlays.default ]; })
          pmod
          lmod
        ];
      in
      {

      #toast = nixpkgs.lib.nixosSystem {
      #  system = "x86_64-linux";
      #  specialArgs = {
      #    inherit inputs self;
      #  };
      #  modules = defaultImports ++ [
      #    ./hosts/toast
      #  ];

      #};

      adelskronen = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit inputs self machines networks connectivity;
        };
        modules = defaultImports ++ [
          inputs.sops-nix.nixosModules.sops
          lmods.phppgadmin
          lmods.meta-network
          lmods.meta-wireguard
          #layers.nixosModules.meta-wg-server
          ./hosts/adelskronen
        ];

      };

      #nanopi = nixpkgs.lib.nixosSystem {
      #  system = "aarch64-linux";
      #  specialArgs = {
      #    inherit inputs self machines networks connectivity;
      #  };
      #  modules = defaultImports ++ [
      #    nanopi.nixosModules.default
      #    nanopi.nixosModules.nanopi-r4s-led
      #    nanopi.nixosModules.nanopi-r4s-baseline
      #    lmods.phppgadmin
      #    lmods.meta-dhcpd
      #    lmods.meta-unbound
      #    lmods.meta-wireguard
      #    ./ops/living-systems/nanopi
      #  ];

      #};
    };

    templates = {
      infra = {
        path = ./.;
        description = "All of infra";
      };

      toastops = {
        path = ./ops/toastops;
        description = "toastops";
      };
    };

  };
}
